import QtQuick 2.4

Rectangle {
    property alias text: tLabel.text
    property alias textColor: tLabel.color
    property alias topColor: colorTop.color
    property alias bottomColor: colorBottom.color

    id: rectRoot
    width: 100
    height: 40
    color: "#ffffff"
    radius: 20
    border.color: "#000000"

    gradient: Gradient {
        GradientStop {
            id: colorTop
            position: 0
            color: "#ffffff"
        }

        GradientStop {
            id: colorBottom
            position: 1
            color: "#aaaaaa"
        }
    }
    border.width: 1

    Text {
        id: tLabel
        text: qsTr("Label")
        anchors.fill: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.WordWrap
    }
}

