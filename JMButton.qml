import QtQuick 2.4

JMButtonForm {

    signal clicked;

    mouseArea.onPressed: {
        colorSave = topColor
        topColor = bottomColor
        bottomColor = colorSave
    }

    mouseArea.onReleased: {
        bottomColor = topColor
        topColor = colorSave
    }

    mouseArea.onClicked: {
        clicked();
    }

    mouseArea.onEnabledChanged: {
        if (enabled) {
            textColor = "#000000"
        } else {
            textColor = "#dddddd"
        }
    }
}
