import QtQuick 2.4

Rectangle {
    id: rRoot
    width: 90
    height: 90
    color: colorUnclick
    radius: 10
    border.width: 1

    property alias color: rRoot.color
    property alias mouse: mouse
    property alias source: image.source
    property string colorClick: "#2222ff"
    property string colorUnclick: "#ff2222"

    MouseArea {
        id: mouse
        anchors.fill: parent

        Image {
            id: image
            anchors.fill: parent
            anchors.margins: 5
            fillMode: Image.PreserveAspectFit
        }
    }
    states: [
        State {
            name: "click"

            PropertyChanges {
                target: rRoot
                color: colorClick
            }
        }
    ]
}
